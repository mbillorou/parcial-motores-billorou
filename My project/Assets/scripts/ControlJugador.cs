using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public Camera camaraPrimeraPersona;
    private Rigidbody rb;
    public float rapidezDesplazamiento = 1;
    private int salto = 0;
    public float magnitudSalto;
    private bool doble;
    public CapsuleCollider col;
    public LayerMask capaPiso;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;


        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(0); Time.timeScale = 1;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (piso())
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            }
            else if (doble)
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
                rb.velocity = Vector3.zero;
                doble = false;
            }
        }

        if (piso())
        {
            doble = true;  
        }

        if (Input.GetKey(KeyCode.R)) {
            SceneManager.LoadScene(0); Time.timeScale = 1;
        } 


    }
    private bool piso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("moneda") == true)
        {
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Afuera") == true)
        {
            SceneManager.LoadScene(0);

        }
    }
}
    


